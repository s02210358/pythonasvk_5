def gen():
    i, res = 1, 1 
    while True:
        yield res
        i += 1
        res += (1 / (i*i))


