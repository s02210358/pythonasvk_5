def travel(n):
    while n:
        yield "по кочкам"
        n -= 1
    return "и в яму"

def travelwrap(n):
    res = yield from travel(n)
    yield res

print(*list(travelwrap(int(input()))), sep='\n')
