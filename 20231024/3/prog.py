import itertools as it
print(*sorted(filter(lambda x: True if x.count("TOR") == len(x) // 3 else False, {"".join(i) for i in list(it.product('TOR', repeat=int(input())))})), sep=', ')
