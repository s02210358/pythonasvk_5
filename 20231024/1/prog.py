import itertools as it

def fib_gen():
	yield 1
	yield 1
	cur, prev = 1, 1
	while True:
		tmp = cur
		cur += prev
		yield cur
		prev = tmp

def fib(m, n):
	res = fib_gen()
	return list(it.islice(res, m, m + n))

import sys
exec(sys.stdin.read())