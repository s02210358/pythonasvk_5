import itertools as it

def slide(seq, n):
	k = 0
	seq_cur, seq_save = it.tee(seq, 2)
	res, check = it.tee(it.islice(seq_cur, k, k + n), 2)
	while list(check):
		yield from res
		k += 1
		seq = seq_save
		seq_cur, seq_save = it.tee(seq, 2)
		res, check = it.tee(it.islice(seq_cur, k, k + n), 2)

import sys
exec(sys.stdin.read())
		