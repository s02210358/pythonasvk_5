import pprint

a = list(eval(input()))

for i in range(len(a)):
	swap = False

	for j in range(0, len(a) - i - 1):
		if (a[j] ** 2) % 100 > (a[j + 1] ** 2) % 100:
			a[j], a[j + 1] = a[j + 1], a[j]
			swap = True
	
	if not swap:
		break

print(a)
