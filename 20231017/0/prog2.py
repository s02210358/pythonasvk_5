import timeit

str_in = input()

def f(str_in):
    glas = set("euioa")
    sogl = set("qywrtpsdfghjklzxcvbnm")
    
    return len(set(str_in) & glas), len(set(str_in) & sogl)

print(timeit.Timer('f(str_in)', globals = globals()).autorange())
