from collections import Counter
import timeit


def counter(s):
    return Counter(s).most_common()

def dicts(s):
    d = {}

    for i in s:
        if d.get(i):
             d[i] += 1
        else:
             d[i] = 1

    return [i for i in d.items()]

s = input().split()

print(timeit.timeit("counter(s)", globals = globals()))
print(timeit.timeit("dicts(s)", globals = globals()))
print(counter(s), dicts(s), sep = '\n')

