import timeit

t = timeit.Timer('text.find(char)', setup='text = "sample string"; char = "g"')
timeit.Timer.autorange(t)

