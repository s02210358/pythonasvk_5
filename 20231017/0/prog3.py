s = input().split()

d = {}

for i in s:
    if d.get(i):
        d[i] += 1
    else:
        d[i] = 1

for i in d.items():
    print(i)
