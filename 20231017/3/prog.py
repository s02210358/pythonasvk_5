from collections import Counter
n = int(input())
res = Counter([])

while(line := input().lower().strip()):
	buff = ""

	for i in range(len(line)):
		if not line[i].isalpha():
			buff += ' '
		else:
			buff += line[i]

	buff = buff.split()
	res += Counter([buff[i] for i in range(len(buff)) if len(buff[i]) == n])

res = [i[0] for i in res.most_common() if i[1] == (res.most_common())[0][1]]
if res:
	print(*res)
