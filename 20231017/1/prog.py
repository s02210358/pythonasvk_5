input_str = input()
j = 0

while j < len(input_str) and not input_str[j].isalpha():
	j += 1

if j < len(input_str):
	prev = input_str[j].lower()
flag = 1
pair_set = set()

for i in range(j + 1, len(input_str)):
	if flag and (cur := input_str[i].lower()).isalpha():
		pair_set.add(prev + cur)
		prev = cur
	elif not flag and (cur := input_str[i].lower()).isalpha():
		flag = 1
		prev = cur
	else:
		flag = 0

print(len(pair_set))
