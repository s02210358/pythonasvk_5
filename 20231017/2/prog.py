from math import *

def pars_params(string):
	flag = 0
	buff = ""

	for i in range(len(string)):
		if not flag and string[i].isspace():
			buff += ', '
		else:
			if (string[i] == '\'' or string[i] == '\"') and not flag:
				flag = 1
			elif (string[i] == '\'' or string[i] == '\"') and flag:
				flag = 0

			buff += string[i]

	if ', ' in buff:
		return eval(buff)
	else:
		return [eval(buff)]


cnt_functions, cnt_lines = 0, 0
funcs = {}

while True:
	params = input()
	cnt_lines += 1

	if ('quit' in params):
		params = params.split(maxsplit = 1)
		print(params[1].format(cnt_functions, cnt_lines))
		break
	elif(params[0][0] == ':'):
		params = params.split()
		funcs[params[0][1:]] = params[1:]
		cnt_functions += 1
	else:
		params = params.split(maxsplit = 1)
		arr = funcs[params[0]]

		if len(params) > 1:
			params = pars_params(params[1])

		print(eval(arr[-1], globals().update({arr[i]: params[i] for i in range(0, len(params))})))
