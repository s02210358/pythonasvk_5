container = []

while (str_in := input()):
	container.append(str_in)

width = len(container[0]) - 2
hight = len(container) - 2
cnt_g, cnt_w = 0, 0

for i in range(1, len(container) - 1):
	if container[i][1] == '.':
		cnt_g += width
	elif container[i][1] == '~':
		cnt_w += width

width, hight = hight, width

g_lines = int(cnt_g / width)
w_lines = hight - g_lines

gas_level = ('\n'.join([('#' + '.' * width + '#') for i in range(g_lines)]) + '\n') if g_lines != 0 else ''
water_level = ('\n'.join([('#' + '~' * width + '#') for i in range(w_lines)]) + '\n') if w_lines != 0 else ''

print('#' * (width + 2) + '\n' + gas_level + water_level + '#' * (width + 2))

cnt_g = g_lines * width
cnt_w = w_lines * width

if cnt_g < cnt_w:
	tmp_1, tmp_2 = cnt_g, cnt_w
	str_1, str_2 = ".", "~"
else:
	tmp_2, tmp_1 = cnt_g, cnt_w
	str_2, str_1 = ".", "~"

res_str = f'{tmp_2}/{tmp_1 + tmp_2}'
print(f'{(str_1 * round(20 * tmp_1 / tmp_2)):<20}', f'{f"{tmp_1}" + "/" + f"{tmp_1 + tmp_2}":>{len(res_str)}}')
print(f'{str_2 * 20}', res_str)
