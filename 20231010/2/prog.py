from math import *

def scale(a, b, A, B, x):
    return ((B - A) / (b - a)) * (x - a) + A

def value_area(params):
	func = lambda x: eval(params[4])
	x = scale(0, params[0] - 1, params[2], params[3], 0)
	min_y, max_y = func(x), func(x)

	for i in range(1, params[0]):
		x = scale(0, params[0] - 1, params[2], params[3], i)
		y = func(x)

		if y < min_y:
			min_y = y
		elif y > max_y:
			max_y = y

	return max_y, min_y


params = input().split()

for i in range(len(params) - 1):
	params[i] = eval(params[i])

func = lambda x: eval(params[-1])

scr = [[' '] * params[0] for i in range(params[1])]

y_max, y_min = value_area(params)
# y_min = int(y_min) if y_min > 0 else int(y_min) - 1
# y_max = int(y_max) + 1 if y_max > 0 else int(y_max)
y_min, y_max = round(y_min), round(y_max)

for i in range(0, params[0] - 1):
	x_cur = scale(0, params[0] - 1, params[2], params[3], i)
	x_next = scale(0, params[0] - 1, params[2], params[3], i + 1)
	h_cur = round(scale(y_max, y_min, 0, params[1] - 1, func(x_cur)))
	h_next = round(scale(y_max, y_min, 0, params[1] - 1, func(x_next)))
	scr[h_next][i + 1] = '*'

	while h_cur != h_next:
		scr[h_cur][i] = '*'
		h_cur += abs(h_next - h_cur) // (h_next - h_cur)

print("\n".join(["".join(s) for s in scr]))
