def scale(a, b, A, B, x):
    return ((B - A) / (b - a)) * (x - a) + A 

from math import *

scr = [[' '] * 60 for i in range(20)]

for i in range(0, 60):
    x = scale(0, 59, -5, 5, i)
    scr[int(scale(0, 2, 0, 19, sin(x) + 1))][i] = "*"

print("\n".join(["".join(s) for s in scr]))
