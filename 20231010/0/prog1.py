import decimal

def esum(N, one):
    t = type(one)
    s = t(0)
    frac = one
    for i in range(N):
        if i != 0:
            frac *= t(i)
        s += one / frac
    return s

decimal.prec = 1000
print(esum(eval(input()), decimal.Decimal(1)))

