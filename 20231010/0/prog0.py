import decimal
import fractions

def multiplier(x, y, Type):
    a = Type(x)
    b = Type(y)
    return a * b

print(multiplier(input(), input(), float))
print(multiplier(input(), input(), decimal.Decimal))
print(multiplier(input(), input(), fractions.Fraction))
