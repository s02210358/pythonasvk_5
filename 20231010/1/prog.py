import fractions as fr

def is_solution(income):
	i = 4
	s1 = income[3]

	while i < 4 + income[2]:
		s1 *= income[0]
		s1 += income[i]
		i += 1

	i += 2

	s2 = income[i - 1]

	while i < len(income):
		s2 *= income[0]
		s2 += income[i]
		i += 1

	return s1 / s2 == income[1] if s2 != 0 else False

income = input().split(",")
for i in range(len(income)):
		income[i] = fr.Fraction(income[i])
print(is_solution(income))
