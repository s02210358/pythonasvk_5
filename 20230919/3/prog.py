a, i = int(input()), 0

while i < 9:
	b, c = a + i // 3, a + i % 3
	p, s = b * c, 0
	tmp = p

	while  tmp != 0:
		s += tmp % 10
		tmp //= 10;

	print(b, "*", c, "=", end = " ")
	print(p, end = " ") if s != 6 else print(":=)", end = " ")
	if i % 3 == 2: print("")

	i += 1
