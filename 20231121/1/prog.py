import sys

std_in = sys.stdin.buffer
std_out = sys.stdout.buffer

if N := std_in.read(1):
	data = std_in.read()
	arr, i = [N], 0
	L = len(data)

	while i < N[0]:
		cur = round(i * L/N[0])
		nxt = round((i + 1) * L/N[0])
		arr.append(data[cur:nxt])
		i += 1

	arr[1:] = sorted(arr[1:])
	std_out.write(b''.join(arr))
