import sys
import pickle
std_in = sys.stdin.buffer

res = {'Size': 0, 'Type': 0, 'Channels': 0, 'Rate': 0, 'Bits': 0, 'Data size': 0}

def check_format(std_in = std_in, res = res):

	if (data:= std_in.read(4)) and data.decode() == 'RIFF': pass
	else:
		return 'NO'

	if (data:=std_in.read(4)):
		res['Size'] = int.from_bytes(data, 'little')
	else:
		return 'NO'

	if (data:= std_in.read(4)) and data.decode() =='WAVE': pass
	else:
		return 'NO'

	std_in.read(8)

	if (data:= std_in.read(2)):
		res['Type'] = int.from_bytes(data, 'little')
	else:
		return 'NO'

	if (data:= std_in.read(2)):
		res['Channels'] = int.from_bytes(data, 'little')
	else:
		return 'NO'

	if (data:= std_in.read(4)):
		res['Rate'] = int.from_bytes(data, 'little')
	else:
		return 'NO'

	std_in.read(6)

	if (data:= std_in.read(2)):
		res['Bits'] = int.from_bytes(data, 'little')
	else:
		return 'NO'

	if (data:= std_in.read(4)) and data.decode() =='data': pass
	else:
		return 'NO'

	if (data:= std_in.read(4)):
		res['Data size'] = int.from_bytes(data, 'little')
	else:
		return 'NO'

	return res


rt = check_format()

if rt == 'NO':
	print(rt)
else:
	arr = []
	for key, value in rt.items():
		arr.append(f'{key}={value}')

	print(', '.join(arr))