import pickle
class SerCls:
    lst = [0, 1, 2]
    dct = {'a': 1, 'v': 2}
    num = 123
    st = "qwerty"

ser = SerCls()
string_in = pickle.dumps(ser)
del ser
ser1 = pickle.loads(string_in)
print(ser1.lst, ser1.dct, ser1.num, ser1.st, sep='\n')
