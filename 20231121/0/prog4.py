import random
import struct

with open('out', 'wb') as f:
    for i in range(10):
        random.seed(12)
        tmp = struct.pack("<f3si",random.random() , bytearray((random.randint(0, 128), random.randint(0, 128), random.randint(0, 128))), random.randint(0, 10000))
        f.write(tmp)

with open('out', 'rb') as f:
    pass
