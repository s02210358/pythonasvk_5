def obj_diff(a, b):
	if "__sub__" in dir(a):
		return a - b
	elif type(a) is type(b):
		return type(a)([i for i in a if i not in b])

print(obj_diff(*eval(input())))