def fun(a, b):
    def f(x):
        return a(x) + b(x)
    return f
a = fun(max, min)
print(a([1,2]))
