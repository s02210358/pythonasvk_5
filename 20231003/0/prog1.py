def fun(*args):
    return sum(args) / len(args)

print(fun(1, 2, 3))
