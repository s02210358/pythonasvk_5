class A: pass
class B: pass
class C(A, B): pass
class D(B, A): pass

class E(A, C): pass
class E(B, C): pass
class E(D, C): pass

class E(C, A): pass #fix first
class E(C, B): pass #fix second

class C(B, A): pass #fix third
class E(C, D): pass
