class A:
    def __str__(self):
        return "A"
class B(A):
    def __str__(self):
        pred = super().__str__()
        return pred + ":" + "B"

class C(B):
    def __str__(self):
        pred = super().__str__()
        return pred + ":" + "C"


print(C())
print(B())
print(A())

