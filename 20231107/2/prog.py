from math import sqrt
class InvalidInput(Exception): pass
class BadTriangle(Exception): pass

def triangleSquare(string):
	try:
		(x1, y1), (x2, y2), (x3, y3) = eval(string)
	except Exception:
		raise InvalidInput

	for i in (x1, x2, x3, y1, y2, y3):
		if (type(i) is not int) and (type(i) is not float):
			raise BadTriangle

	a = sqrt((x2 - x1)**2 + (y2 - y1)**2)
	b = sqrt((x3 - x2)**2 + (y3 - y2)**2)
	c = sqrt((x1 - x3)**2 + (y1 - y3)**2)

	if a + b == c or a + c == b or b + c == a:
		raise BadTriangle

	if min(a, b, c) <= 0 or 2 * max(a, b, c) > sum((a, b, c)):
		raise BadTriangle

	p = (a + b + c) / 2
	return sqrt((p - a)*(p - b)*(p - c)*p)

while True:
	instr = input()
	try:
		res = triangleSquare(instr)
	except InvalidInput:
		print("Invalid input")
	except BadTriangle:
		print("Not a triangle")
	else:
		print(f'{res:0.2f}')
		break
