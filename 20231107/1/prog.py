from collections import UserString

class DivStr(UserString):
	def __init__(self, value = ''):
		super().__init__(value)
	def __floordiv__(self, value):
		string = str(self)
		arr, i = [], 0
		len_seq = len(string) // value

		while i < value:
			buf = ''
			j = i * len_seq
			k = 0

			while k < len_seq:
				buf += string[j]
				k += 1
				j += 1
			
			arr.append(buf)
			i += 1

		return iter(arr)

	def __mod__(self, value):
		div = ''.join(self // value)
		string = str(self)
		
		return self.__class__(string.removeprefix(div))

import sys
exec(sys.stdin.read())