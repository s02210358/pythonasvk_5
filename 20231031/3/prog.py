from collections import defaultdict

class Maze:
    def __init__(self, x):
        self.size = x
        self.open = defaultdict(set)
        for i in range(x):
            for j in range(x):
                self.open[(i, j)] = set()

    def __setitem__(self, key, value):
        a = [key[0], key[1].start]
        b = [key[1].stop, key[2]]
        if a[0] == b[0] and a[1] != b[1]:
            if a[1] > b[1]:
                a[1], b[1] = b[1], a[1]

            if value == '·':
                for i in range(a[1], b[1]):
                    self.open[(a[0], i)].add((a[0], i + 1))
                    self.open[(a[0],i + 1)].add((a[0], i))
            if value == '█':
                for i in range(a[1], b[1] + 1):
                    if (a[0] + 1, i) in self.open.keys():
                        self.open[(a[0] + 1, i)].discard((a[0], i))
                    elif (a[0] - 1, i) in self.open.keys():
                        self.open[(a[0] - 1, i)].discard((a[0], i))
                    self.open[(a[0], i)].clear()

                if (a[0], a[1] - 1) in self.open.keys():
                    self.open[(a[0], a[1] - 1)].discard((a[0], a[1]))
                elif (a[0], a[1] + 1) in self.open.keys():
                    self.open[(a[0], a[1] + 1)].discard((a[0], a[1]))

                if (a[0], b[1] - 1) in self.open.keys():
                    self.open[(a[0], b[1] - 1)].discard((b[0], a[1]))
                elif (a[0], b[1] + 1) in self.open.keys():
                    self.open[(a[0], b[1] + 1)].discard((b[0], a[1]))
        elif a[0] != b[0] and a[1] == b[1]:
            if a[0] > b[0]:
                a[0], b[0] = b[0], a[0]

            if value == '·':
                for i in range(a[0], b[0]):
                    self.open[(i, a[1])].add((i + 1, a[1]))
                    self.open[(i + 1, a[1])].add((i, a[1]))
            if value == '█':
                for i in range(a[0], b[0] + 1):
                    if (i, a[1] + 1) in self.open.keys():
                        self.open[(i, a[1] + 1)].discard((i, a[1]))
                    elif (i, a[1] - 1) in self.open.keys():
                        self.open[(i, a[1] - 1)].discard((i, a[1]))
                    self.open[(i, a[1])].clear()

                if (a[0] - 1, a[1]) in self.open.keys():
                    self.open[(a[0] - 1, a[1])].discard((a[0], a[1]))
                elif (a[0] + 1, a[1]) in self.open.keys():
                    self.open[(a[0] + 1, a[1])].discard((a[0], a[1]))

                if (b[0] - 1, a[1]) in self.open.keys():
                    self.open[(b[0] - 1, a[1])].discard((b[0], a[1]))
                elif (b[0] + 1, a[1]) in self.open.keys():
                    self.open[(b[0] + 1, a[1])].discard((b[0], a[1]))

    def __getitem__(self, key):
        enter = (key[0], key[1].start)
        exit = (key[1].stop, key[2])

        prev = set([enter])
        total = prev

        while True:
            cur = set()

            for el in prev:
                cur |= self.open[el]

            if not(cur - total):
                break
            else:
                total |= cur
                prev = cur

        if exit in total:
            return True
        else:
            return False

    def __str__(self):
        msh = lambda x: 2*x + 1
        dec = f'{"█" * (msh(self.size))}' + '\n'

        for j in range(self.size):
            buf1 = "█"
            buf2 = "█"

            for i in range(self.size):
                buf1 += '·'

                if (i + 1, j) in self.open[(i, j)]:
                    buf1 += '·'
                else:
                    buf1 += '█'

                if (i, j + 1) in self.open[(i, j)]:
                    buf2 += '·'
                else:
                    buf2 += '█'

                buf2 += '█'

            dec += buf1 + '\n' + buf2 + '\n'

        return dec.strip('\n')


import sys
exec(sys.stdin.read())
