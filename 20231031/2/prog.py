from math import sqrt

class Triangle:
	def __init__(self, a, b, c):
			self.x, self.y, self.z = tuple(a), tuple(b), tuple(c)
			self.a = sqrt((self.x[0] - self.y[0])**2 + (self.x[1] - self.y[1])**2)
			self.b = sqrt((self.y[0] - self.z[0])**2 + (self.y[1] - self.z[1])**2)
			self.c = sqrt((self.z[0] - self.x[0])**2 + (self.z[1] - self.x[1])**2)
	def __bool__(self):
		if abs(self):
			return True
		else:
			return False
	def __abs__(self):
		if self.x[0] == self.y[0] == self.z[0] or self.x[1] == self.y[1] == self.z[1]:
			return 0
		elif not(min(self.a, self.b, self.c) > 0 and 2 * max(self.a, self.b, self.c) < sum((self.a, self.b, self.c))):
			return 0
		else:
			p = (self.a + self.b + self.c) / 2
			return sqrt((p-self.a)*(p-self.b)*(p-self.c)*p)
	def __lt__(self, obj):
		return abs(self) < abs(obj)
	def __contains__(self, obj):
		if self is obj or not self or not obj:
			return True

		a_z = lambda x: x >= 0
		res = []

		for i in (obj.x, obj.y, obj.z):
			tmp_1 = (self.x[0] - i[0])*(self.y[1] - i[1]) - (self.y[0] - i[0])*(self.x[1] - i[1])
			tmp_2 = (self.y[0] - i[0])*(self.z[1] - i[1]) - (self.z[0] - i[0])*(self.y[1] - i[1])
			tmp_3 = (self.z[0] - i[0])*(self.x[1] - i[1]) - (self.x[0] - i[0])*(self.z[1] - i[1])

			if a_z(tmp_1) == a_z(tmp_2) == a_z(tmp_3):
				res.append(True)
			else:
				res.append(False)

		return all(res)
	def __and__(self, obj):
		if not self or not obj:
			return False

		a_z = lambda x: x >= 0

		for i in (self.x, self.y, self.z):
			if i in (obj.x, obj.y, obj.y):
				return True

		for j, k, m in ((self.x, self.y, self.z), (self.y, self.z, self.x), (self.x, self.z, self.y)):
			tmp_0 = (j[0] - m[0])*(k[1] - m[1]) - (k[0] - m[0])*(j[1] - m[1])
			tmp_1 = (j[0] - obj.x[0])*(k[1] - obj.x[1]) - (k[0] - obj.x[0])*(j[1] - obj.x[1])
			tmp_2 = (j[0] - obj.y[0])*(k[1] - obj.y[1]) - (k[0] - obj.y[0])*(j[1] - obj.y[1])
			tmp_3 = (j[0] - obj.z[0])*(k[1] - obj.z[1]) - (k[0] - obj.z[0])*(j[1] - obj.z[1])

			if 0 in (arr := [tmp_3, tmp_2, tmp_1]):
				arr.remove(0)
				if a_z(arr[0]) == a_z(arr[1]) and a_z(arr[0]) != a_z(tmp_0):
					return False
			else:
				if a_z(tmp_1) == a_z(tmp_2) == a_z(tmp_3) and a_z(tmp_1) != a_z(tmp_0):
					return False

		for j, k, m in ((obj.x, obj.y, obj.z), (obj.y, obj.z, obj.x), (obj.x, obj.z, obj.y)):
			tmp_0 = (j[0] - m[0])*(k[1] - m[1]) - (k[0] - m[0])*(j[1] - m[1])
			tmp_1 = (j[0] - self.x[0])*(k[1] - self.x[1]) - (k[0] - self.x[0])*(j[1] - self.x[1])
			tmp_2 = (j[0] - self.y[0])*(k[1] - self.y[1]) - (k[0] - self.y[0])*(j[1] - self.y[1])
			tmp_3 = (j[0] - self.z[0])*(k[1] - self.z[1]) - (k[0] - self.z[0])*(j[1] - self.z[1])

			if 0 in (arr := [tmp_3, tmp_2, tmp_1]):
				arr.remove(0)
				if a_z(arr[0]) == a_z(arr[1]) and a_z(arr[0]) != a_z(tmp_0):
					return False
			else:
				if a_z(tmp_1) == a_z(tmp_2) == a_z(tmp_3) and a_z(tmp_1) != a_z(tmp_0):
					return False

		return True

import sys
exec(sys.stdin.read())
