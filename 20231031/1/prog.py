class Omnibus:
	attr = {}

	def __setattr__(self, name, value):
		self.attr.setdefault(name, set()).add(self)
	def __delattr__(self, name):
		if name in self.attr.keys() and self in self.attr[name]:
			self.attr[name].remove(self)
	def __getattr__(self, name):
		if name in self.attr.keys() and self in self.attr[name]: 
			return len(self.attr[name])

import sys
exec(sys.stdin.read())