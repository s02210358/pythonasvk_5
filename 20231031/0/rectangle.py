class Rectangle:
    rectcnt = [0]

    def __init__(self, x1, y1, x2, y2):
        self.x1 = x1
        self.x2 = x2
        self.y1 = y1
        self.y2 = y2
        self.rectcnt[0] += 1
        setattr(self, f"rect_{self.rectcnt[0]}", self.rectcnt[0])
    def __str__(self):
        return f"({self.x1},{self.y1})({self.x1},{self.y2})({self.x2},{self.y2})({self.x2},{self.y1}) -- rectcnt == {self.rectcnt[0]}"
    def __abs__(self):
        return (self.x2 - self.x1)*(self.y2 - self.y1)
    def __lt__(self, obj):
        return abs(self) < abs(obj)
    def __eq__(self, obj):
        return abs(self)  == abs(obj)
    def __mul__(self, m):
        return self.__class__(self.x1 * m, self.y1 * m, self.x2 * m, self.y2 * m)
    __rmul__ = __mul__
    def __getitem__(self, idx):
        a = [(self.x1, self.y1), (self.x1, self.y2), (self.x2, self.y2), (self.x2, self.y1)]
        return a[idx]
    def __bool__(self):
        return abs(self) != 0
    def __del__(self):
        print(f"Deleting", self)
        self.rectcnt[0] -= 1


a = Rectangle(1, 1, 4, 4)
b = Rectangle(2, 2, 3, 3)
c = Rectangle(1, 2, 3, 4)
#print(dir(a), dir(b), dir(c), sep='\n\n')
#print(str(c))
#print(2 * a, sep='\n')
del b
for i in a:
    print(i)

