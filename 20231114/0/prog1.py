def typecheck(f):
    def newfun(*args):
        for i in args:
            if type(i) is not int:
                raise TypeError
        return f(*args)
    return newfun

@typecheck
def mult(a, b):
    return a * b


print(mult(3, 4))
print(mult(1.2, 3.3))

