def type(a):    
    def typecheck(f):
        def newfun(*args):
            for i in args:
                if not isinstance(i, a):
                    raise TypeError
            return f(*args)
        return newfun
    return typecheck

@type(float)
def mult(a, b):
    return a * b

print(mult(1.1, 3.3))
print(mult(2, 3))

