class Sender:
    flag = True

    @classmethod
    def report(self, obj):
        if self.flag:
            print("Greetings")
            self.flag = False
        else:
            print("Get away")


class Asker:
    @staticmethod
    def askall(lst):
        for i in lst:
            Sender.report(i)


Asker().askall([Sender() for i in range(3)])
