class Alpha:
    __slots__ = [i for i in "abcdefghijklmnopqrstuvwxyz"]

    def __init__(self, **args):
        for key, value in args.items():
            setattr(self, key, value)

    def __str__(self):
        out = []

        for i in self.__slots__:
            if (j := getattr(self, i, '')):
                out.append(f'{i}: {j}')

        return ', '.join(out)

class AlphaQ:
    arr = [i for i in "abcdefghijklmnopqrstuvwxyz"]

    def __init__(self, **args):
        for key, value in args.items():
            setattr(self, key, value)

    def __str__(self):
        out = []

        for i in self.arr:
            if (j := getattr(self, i, '')):
                out.append(f'{i}: {j}')

        return ', '.join(out)


import sys
exec(sys.stdin.read())
