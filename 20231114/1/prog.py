def objcount(clss):
    class Newclass(clss):
        counter = 0
        def __init__(self):
            super().__init__()
            self.__class__.counter += 1
        def __del__(self):
            if self.__class__.counter > 0:
                self.__class__.counter -= 1
            if hasattr(super(), '__del__'):
                super().__del__()

    return Newclass

import sys
exec(sys.stdin.read())