class Num:
    def __get__(self, obj, cls):
        if '_val' in dir(obj):
            return obj._val
        else:
            return 0
    def __set__(self, obj, value):
        if hasattr(value, 'real'):
            obj._val = value
        elif hasattr(value, '__len__'):
            obj._val = len(value)

import sys
exec(sys.stdin.read())