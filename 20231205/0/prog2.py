import asyncio

async def squarer(val):
    return val * val

async def doubler(val):
    return 2 * val

async def main():
    x, y = eval(input())
    x, y = await asyncio.gather(squarer(x), squarer(y))
    x, y = await asyncio.gather(doubler(x), doubler(y))

    print(list((x, y)))

asyncio.run(main())
