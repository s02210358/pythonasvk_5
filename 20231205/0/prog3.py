import asyncio

async def snd(evsnd):
    evsnd.set()
    print('<snd>:generated <evsnd>')

async def mid1(evsnd, evmid1):
    await evsnd.wait()
    print('<mid1>: received <evsnd>')
    evmid1.set()
    print('<mid1>:generated <evmid1>')


async def mid2(evsnd, evmid2):
    await evsnd.wait()
    print('<mid2>: received <evsnd>')
    evmid2.set()
    print('<mid2>:generated <evmid2>')


async def rcv(evmid1, evmid2):
    await evmid1.wait()
    print('<rcv>: received <evmid1>')
    await evmid2.wait()
    print('<rcv>: received <evmid2>')

async def main():
    events = [asyncio.Event() for i in range(3)]
    res = await asyncio.gather(snd(events[0]), mid1(events[0], events[1]), mid2(events[0], events[2]), rcv(events[1], events[2]))

asyncio.run(main())

