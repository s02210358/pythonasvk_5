import asyncio

async def  prod(q1):
    for i in range(5):
        await q1.put(f'value_{i}')
        print(f'<prod>:put <{i}> to q1')
        await asyncio.sleep(1)

async def mid(q1, q2):
    while True:
        item = await q1.get()
        print(f'<mid>:got <{item}> from <q1>')
        await q2.put(item)
        print(f'<mid>:put <{item}> to <q2>')

async def cons(q2):
    while True:
        item = await q2.get()
        print(f'<cons>:got <{item}> from <q2>')

async def main():
    qs = [asyncio.Queue() for i in range(2)]
    await asyncio.gather(prod(qs[0]), mid(qs[0], qs[1]), cons(qs[1]))

asyncio.run(main())
