import asyncio
from collections import deque
evn = asyncio.Event()

async def writer(q, late):
	cnt = 0	
	while not evn.is_set():
		await asyncio.sleep(late)
		await q.put(f'{cnt}_{late}')
		cnt += 1

async def stacker(q, stack):
	while not evn.is_set():
		item = await q.get()
		await stack.put(item)

async def reader(stack, numb, late):
	for i in range(numb):
		await asyncio.sleep(late)
		print(await stack.get())

	evn.set()

async def main():
	data = eval(input())
	q = asyncio.Queue()
	s = asyncio.Queue()
	await asyncio.gather(writer(q, data[0]), writer(q, data[1]), stacker(q, s), reader(s, data[3], data[2]))

asyncio.run(main())