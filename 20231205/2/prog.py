import asyncio
import random

async def merge(A, B, s, m, f, e_in1, e_in2, e_out):
    await e_in1.wait()
    await e_in2.wait()

    n1 = m - s + 1
    n2 = f - m
    L = [0] * (n1)
    R = [0] * (n2)
 
    for i in range(0, n1):
        L[i] = A[s + i]
 
    for j in range(0, n2):
        R[j] = A[m + 1 + j]
 
    i = 0     
    j = 0     
    k = s
 
    while i < n1 and j < n2:
        if L[i] <= R[j]:
            B[k] = L[i]
            i += 1
        else:
            B[k] = R[j]
            j += 1
        k += 1
 
    while i < n1:
        B[k] = L[i]
        i += 1
        k += 1
 
    while j < n2:
        B[k] = R[j]
        j += 1
        k += 1

    e_out.set()

async def mtasks(A_ttt):
    A = A_ttt.copy()
    res = []
    l_A = len(A)
    E_in = [asyncio.Event() for i in range(2 * len(A))]
    f = 1
    s = 0

    for i in E_in:
        i.set()

    while (f - s) < l_A:
        E_out = [asyncio.Event() for i in range((l_A // (f - s + 1)))]
        ts, i, tf = 0, 0, f
        B = A

        tmp = tf - ts

        while tf < l_A:
            res.append(asyncio.create_task(merge(A, B, ts, ts + (tf - ts) // 2, tf, E_in[2*i], E_in[2*i+1], E_out[i])))
            ts = tf + 1
            tf = ts + tmp
            i += 1

        if ts < l_A:
            add = asyncio.Event()
            A = B
            ts -= 1 + tmp
            res.append(asyncio.create_task(merge(A, B, ts, ts + (l_A - 1 - ts) // 2, l_A - 1, E_in[-2], E_in[-1], add)))

        E_in = E_out
        A = B
        f += (f + 1)

    return res, A

import sys
exec(sys.stdin.read())
