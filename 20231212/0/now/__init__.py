import time
from pathlib import Path

p = Path('DATE.txt')

if p.exists():
    with open("DATE.txt", "rt") as f:
        DATE = f.read()
else:
    DATE = time.time()

def ext():
    with open("DATE.txt", 'wt') as f:
        f.write(f'{DATE}')

import atexit
atexit.register(ext)

