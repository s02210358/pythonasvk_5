from collections import defaultdict
import math
import sys

def parse(line):
	tmp = line.split()
	return [i for i in tmp if i != ' ']

data = defaultdict(int)
cmd = []
hops = defaultdict(int)
cnt = 0
exz = True

while line := sys.stdin.readline():
	line = parse(line)
	match line:
		case ['stop'|'store'|'add'|'sub'|'div'|'mul'|'ifeq'|'ifne'|'ifge'|'ifgt'|'iflt'|'ifle'|'out' as PO, *args]:
			cmd.append([PO, *args])
			cnt += 1
			if PO in ('ifeq', 'ifne', 'ifge', 'ifgt', 'iflt', 'ifle'):
				if args[-1] not in hops.keys():
					exz = False

		case [hop, *etc]:
			if hop[-1] == ':':
				hops[hop[0:-1]] = cnt
				cmd.append(etc)
				cnt += 1

if exz:
	i = 0
	while i < len(cmd):
		match cmd[i]:
			case ['stop']: break
			case ['store', val, var]:
				value = eval(val)
				if value.real != value:
					value = 0

				data[var] = value
			case ['add' | 'sub' | 'div' | 'mul' as OP, src, opr, wr]:
				match OP:
					case 'add':
						try:
							data[wr] = data[src] + data[opr]
						except Exception:
							data[wr] = math.inf
					case 'sub':
						try:
							data[wr] = data[src] - data[opr]
						except Exception:
							data[wr] = math.inf
					case 'mul':
						try:
							data[wr] = data[src] * data[opr]
						except Exception:
							data[wr] = math.inf
					case 'div':
						try:
							data[wr] = data[src] / data[opr]
						except Exception:
							data[wr] = math.inf
			case ['ifeq'|'ifne'|'ifge'|'ifgt'|'iflt'|'ifle' as cmp, src, opr, mark]:
				match cmp:
					case 'ifeq':
						match data[src] == data[opr]:
							case True:
								i = hops[mark] - 1
							case _: pass
					case 'ifne':
						match data[src] != data[opr]:
							case True:
								i = hops[mark] - 1
							case _: pass
					case 'ifge':
						match data[src] >= data[opr]:
							case True:
								i = hops[mark] - 1
							case _: pass
					case 'ifgt':
						match data[src] > data[opr]:
							case True:
								i = hops[mark] - 1
							case _: pass
					case 'iflt':
						match data[src] < data[opr]:
							case True:
								i = hops[mark] - 1
							case _: pass
					case 'ifle':
						match data[src] <= data[opr]:
							case True:
								i = hops[mark] - 1
							case _: pass
			case ['out', var]:
				print(data[var])
		i += 1