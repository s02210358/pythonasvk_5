class Doubleton(type):
    _instance = [None, None]
    def __call__(cls, *args, **kw):
        if cls._instance[0] is None:
            cls._instance.pop(0)
            cls._instance.append(super().__call__(*args, **kw))
        return cls._instance
