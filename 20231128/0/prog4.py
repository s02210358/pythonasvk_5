class C: pass
C.i1, C.i2, C.i3 = input().split()

while (word := input()) != 'end':
    match word:
        case [C.i1, *els] if 'yes' == C.i1 or 'yes' in els:
            print(1)
        case [C.i2]:
            print(2)
        case [C.i3, *els, C.i2]:
            print(3)
        case _:
            print("ERR")

