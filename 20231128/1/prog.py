class dump(type):
    def __init__(cls, name, parents, ns, **kwds):
        def decor(fun):
            def newfun(obj, *args, **kwds):
                print(f'{fun.__name__}: {args}, {kwds} ')
                return fun(obj, *args, **kwds)
            return newfun

        tmp = list(cls.__dict__.keys())
        i = 1

        while tmp[i] != '__dict__':
            setattr(cls, tmp[i], decor(getattr(cls, tmp[i])))
            i += 1            

        return super().__init__(name, parents, ns)

import sys
exec(sys.stdin.read())